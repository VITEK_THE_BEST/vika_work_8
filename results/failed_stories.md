## happy path 1 (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* greet: hello there!   <!-- predicted: запись_к_врачу: hello there! -->
    - utter_greet   <!-- predicted: событие_записи_к_врачу -->
* mood_great: amazing   <!-- predicted: запись_к_врачу: amazing -->
    - utter_happy   <!-- predicted: событие_записи_к_врачу -->


## happy path 2 (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* greet: hello there!   <!-- predicted: запись_к_врачу: hello there! -->
    - utter_greet   <!-- predicted: событие_записи_к_врачу -->
* mood_great: amazing   <!-- predicted: запись_к_врачу: amazing -->
    - utter_happy   <!-- predicted: событие_записи_к_врачу -->
* goodbye: bye-bye!   <!-- predicted: запись_к_врачу: bye-bye! -->
    - utter_goodbye   <!-- predicted: событие_записи_к_врачу -->


## sad path 1 (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* greet: hello   <!-- predicted: запись_к_врачу: hello -->
    - utter_greet   <!-- predicted: событие_записи_к_врачу -->
* mood_unhappy: not good   <!-- predicted: запись_к_врачу: not good -->
    - utter_cheer_up   <!-- predicted: событие_записи_к_врачу -->
    - utter_did_that_help   <!-- predicted: action_listen -->
* affirm: yes   <!-- predicted: запись_к_врачу: yes -->
    - utter_happy   <!-- predicted: событие_записи_к_врачу -->


## sad path 2 (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* greet: hello   <!-- predicted: запись_к_врачу: hello -->
    - utter_greet   <!-- predicted: событие_записи_к_врачу -->
* mood_unhappy: not good   <!-- predicted: запись_к_врачу: not good -->
    - utter_cheer_up   <!-- predicted: событие_записи_к_врачу -->
    - utter_did_that_help   <!-- predicted: action_listen -->
* deny: not really   <!-- predicted: запись_к_врачу: not really -->
    - utter_goodbye   <!-- predicted: событие_записи_к_врачу -->


## sad path 3 (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* greet: hi   <!-- predicted: запись_к_врачу: hi -->
    - utter_greet   <!-- predicted: событие_записи_к_врачу -->
* mood_unhappy: very terrible   <!-- predicted: запись_к_врачу: very terrible -->
    - utter_cheer_up   <!-- predicted: событие_записи_к_врачу -->
    - utter_did_that_help   <!-- predicted: action_listen -->
* deny: no   <!-- predicted: запись_к_врачу: no -->
    - utter_goodbye   <!-- predicted: событие_записи_к_врачу -->


## say goodbye (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* goodbye: bye-bye!   <!-- predicted: запись_к_врачу: bye-bye! -->
    - utter_goodbye   <!-- predicted: событие_записи_к_врачу -->


## bot challenge (C:\Users\Kross\AppData\Local\Temp\tmpcyph5ued\997a3a5488d04deb99067513ed6b9316_conversation_tests.md)
* bot_challenge: are you a bot?   <!-- predicted: запись_в_детсад: are you a bot? -->
    - utter_iamabot   <!-- predicted: событие_записи_в_детсад -->


