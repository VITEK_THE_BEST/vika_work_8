# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"/

from typing import Any, Text, Dict, List, Union

from rasa_sdk import Action, Tracker
from rasa_sdk.forms import FormAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet,  SessionStarted,  ActionExecuted,  EventType



#ИЗМЕНИТЬ поменять местами заполнение города и секции
# ошибки еще присутствуют и опечатки
class ActionSportSession(FormAction):

    def name(self) -> Text:
        return "запись_на_секцию_form"
        
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """какие поля должны быть заполнены и в каком порядке"""

        return ["section","city","address_section","fio"]
        
    @staticmethod
    def section_db() -> List[Text]:
        return [
            'бокс',
            'футбол',
            'хоккей',
            'аэробика',
            'рисование',
            'бокс',
            'рукопашный',
            'карате',
            'гиревой',
            'баскетбол',
            'волейбол',
        ]

    #cюда нужно добавить фильтрацию строк чтобы когда писали бокса подразумевалось бокс
    def validate_section(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city": "", "section": "", "fio": "","address_section": ""}
        elif value is not None and value.lower() in self.section_db():

            dispatcher.utter_message("""вы выбрали секцию """ + value)
            return {"section": value}
        else:
            dispatcher.utter_message("""вы не выбрали секцию 
Если хотите отменить заполнение напишите "стоп" """ )
            return {"section": None}
    
    def validate_city(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city": "", "section": "", "fio": "","address_section": ""}
        elif value is not None and value is not "":#and value.lower() in self.type_medic_db()
          #добавить поиск ДОСТУПНЫХ секций в ГОРОДЕ и в определенном МЕСТЕ
            if tracker.get_slot("section") not in self.section_db():
                dispatcher.utter_message(f"""в городе {value} нет выбранной вами секции или свободного места в ней
Выберите другую секцию. Вот все доступные секции в городе """)
                for target_list in self.section_db():
                    dispatcher.utter_message(target_list)
                return {"city": value}
            # elif tracker.get_slot("section") in self.section_db():#сюда добавить то что если одно место. то записываем в одно месте если нет то даем ему выбор определенной секции
            #     dispatcher.utter_message("вы записались на секцию" + tracker.get_slot("section") 
            #     + "вот где она находиться " + "какой-то адресс")
            #     return {"city": value,"address_section": "какой-то адресс" }
            else:
                dispatcher.utter_message("""Вот свободные секции в городе и их адресс(нет) """ + value)

                for section in self.section_db():#здесь будет выводиться введенная секция и ее АДРЕСС определенного города 
                    dispatcher.utter_message(section) 
                return {"city": value}
        else:
            dispatcher.utter_message("""вы не выбрали город 
Если хотите отменить заполнение напишите "стоп" """ )
            return {"city": None}

    def validate_address_section(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city": "", "section": "", "fio": "","address_section": ""}
        elif value is not None and  value is not "":
            return {"address_section": value}
        else:
            dispatcher.utter_message("""выберите секцию из списка """)
            return {"address_section": None}

    def validate_fio(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city": "", "section": "", "fio": "","address_section": ""}
        elif value is not None and 2 <= len(value.split(' ')) <= 3:
            return {"fio": value}
        else:
            dispatcher.utter_message("""Введите вашу ФИО (раздельно)
Если хотите отменить заполнение напишите "стоп" """)
            return {"fio": None}

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict[Text, Any]]]]:
        """Эта штука походу нужна для того чтобы записать данные тип вот мы отправляем
        значение null и он не будет высирать значения пока не примит значение """

        return {
            'city': [
                self.from_entity(entity='city'),
                self.from_text()
            ],
            "section": [
                self.from_entity(entity='section'),
                self.from_text()
            ],
            "address_section": [
                self.from_entity(entity='address_section'),
                self.from_text()
            ],
            'fio': [
                self.from_entity(entity='fio'),
                self.from_text()
            ],
        }

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Что делать когда все поля заполнены """

        city = tracker.get_slot('city')
        section = tracker.get_slot('section')
        address_section = tracker.get_slot('address_section')
        fio = tracker.get_slot('fio')

        if city != "" and section != "" and fio != "" and  address_section != "" :
            dispatcher.utter_message("""Готово, вы записаны в {city} на {section}
Aдресс записанной секции {address_section}
Имя записаного: {fio}""".format(
                city=city,
                section=section,
                fio=fio,
                address_section=address_section,
            ))
        else:
            dispatcher.utter_message("""Ну ладно… Спросите что-нибудь другое""")
        
        return [
            SlotSet('city', None),
            SlotSet('section', None),
            SlotSet('fio', None),
            SlotSet('address_section', None)
        ]
##############################################################################



class ActionSportSessionInf(FormAction):

    def name(self) -> Text:
        return "информация_о_секциях_form"
        
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """какие поля должны быть заполнены и в каком порядке"""

        return ["city_inf","section_inf","section_id_inf"]
        
    @staticmethod
    def section_db() -> List[Text]:
        return [
            'бокс',
            'футбол',
            'хоккей',
            'аэробика',
            'рисование',
            'бокс',
            'рукопашный',
            'карате',
            'гиревой',
            'баскетбол',
            'волейбол',
        ]

    def validate_city_inf(#выведет все секции в городе
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city_inf": "", "section_inf": "", "section_id_inf": ""}
        elif value is not None and value is not "":
            dispatcher.utter_message(f"""вот все секции в городе {value}  """)

            for target_list in self.section_db():
                # if город есть в section_db
                dispatcher.utter_message(target_list)
                
                return {"city_inf": value}
        else:
            dispatcher.utter_message("""вы не выбрали город 
Если хотите отменить заполнение напишите "стоп" """ )
            return {"city_inf": None}
        

    #добавить условие перезаписи секции
    def validate_section_inf(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city_inf": "", "section_inf": "", "section_id_inf": ""}
        elif value is not None and value.lower() in self.section_db():

            if value in self.section_db():
                for target_list in self.section_db():
                    # if город есть в section_db
                    dispatcher.utter_message(target_list)
                
                return {"section_inf": value}
            elif value not in self.section_db():
                dispatcher.utter_message("""я не нашла такую секцию в городе """ + tracker.get_slot('city_inf'))
                
        else:
            dispatcher.utter_message("""Если хотите отменить заполнение напишите "стоп" """ )
            return {"section_inf": None}
    

    def validate_section_id_inf(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        if value == "стоп":
            return {"city_inf": "", "section_inf": "", "section_id_inf": ""}
        elif value is not None and  value is not "":
            #сюда добавить поиск по бд в городе (city_inf) по секции (section_inf) через id(value) выведенной секции
            dispatcher.utter_message("""вы хотите продолжить просмотр информации ? """)
            return {"section_id_inf": ""}
        else:
            dispatcher.utter_message("""выберите секцию из списка """)
            return {"section_id_inf": None}


    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict[Text, Any]]]]:
        """Эта штука походу нужна для того чтобы записать данные тип вот мы отправляем
        значение null и он не будет высирать значения пока не примит значение """

        return {
            'city_inf': [
                self.from_entity(entity='city_inf'),
                self.from_text()
            ],
            "section_inf": [
                self.from_entity(entity='section_inf'),
                self.from_text()
            ],
            "section_id_inf": [
                self.from_entity(entity='section_id_inf'),
                self.from_text()
            ],
        }

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Что делать когда все поля заполнены """

        city_inf = tracker.get_slot('city_inf')
        section_inf = tracker.get_slot('section_inf')
        section_id_inf = tracker.get_slot('section_id_inf')

        if city_inf != "" and section_inf != "" and  section_id_inf != "" :
            dispatcher.utter_message("""Готово, вы записаны в {city_inf} на {section_inf}
Aдресс записанной секции {section_id_inf}
Имя записаного: {fio_inf}""".format(
                city_inf=city_inf,
                section_inf=section_inf,
                section_id_inf=section_id_inf,
            ))
        else:
            dispatcher.utter_message("""Вы также можете записаться на секцию, просто введите "хочу на секцию" """)
        
        return [
            SlotSet('city_inf', None),
            SlotSet('address_section', None),
            SlotSet('section_inf', None),
        ]






# class ActionMedicalService(Action):

#     def name(self) -> Text:
#         return "услуги_больницы"

#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
#         return []